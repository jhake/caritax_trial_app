class NgosController < ApplicationController
  def new
    @ngo = Ngo.new
  end
  
  def index
    @ngos = Ngo.all
  end
  
  def show
    @ngo = Ngo.find(params[:id])
  end
  
  def create
    @ngo = Ngo.new(ngo_params)
    if @ngo.save
      flash[:success] = "New NGO Added!"
      redirect_to @ngo
    else
      render 'new'
    end
  end
  
  def donate
    @ngo = Ngo.find(params[:id])
  end
  
  def make_donation

    tokenInput = TokenCreateInput.new
    tokenInput.number = params["card_number"]
    tokenInput.exp_month = params["expiration"].split('/')[0].to_i
    tokenInput.exp_year = params["expiration"].split('/')[1].to_i
    tokenInput.cvc = params["cvc"]
    tokenInput.billing = {
      :line1 => nil,
      :line2 => nil,
      :city => nil,
      :state => nil,
      :postal_code => nil,
      :country => nil,
      :name => params["name"],
      :email => nil,
      :phone => nil
    }
    
    token = TokenObject.new
    if token.create(tokenInput) == false
      p "failed token create"
      #failed
      flash[:danger] = token.errors[0]["detail"]
      redirect_to action: 'donate', id: params["from_ngo"]
      return
    end
    
    paymentInput = PaymentCreateInput.new
    paymentInput.amount = params["amount"].to_i*100
    paymentInput.currency = "PHP"
    paymentInput.source = {
    				"id" => token.id,
    				"type" => "token"
    			}
    paymentInput.description = "Donation to " + Ngo.find(params["from_ngo"]).name + " | " + "Name: " + params["name"] + " | " + "Phone: " + params["phone_number"] + " | " + "TIN: " + params["tin"] +  " | " + "Address: " + params["address"]
    paymentInput.statement_descriptor = "Donataion"
    
    payment = PaymentObject.new
    
    if payment.create(paymentInput) == false
      p "failed payment create"
      #failed
      flash[:danger] = payment.errors[0]["detail"]
      redirect_to action: 'donate', id: params["from_ngo"]
      return
    end

    if Ngo.find(params["from_ngo"]).name == "The University of the Philippines Foundation, Inc."
      p "UP"
      deduction = params["amount"].to_i.*1.50
    else
      p "not UP"
      deduction = params["amount"].to_i
    end
    
    flash[:success] = "SUCCESS. Donated to " + Ngo.find(params["from_ngo"]).name + "." +
                      " Your taxable income will decrease by " + deduction.to_s +
                      " Pesos! Please expect your Certificate of Donation in 30 days. Your accountant will file this along with your tax return to validate your tax deduction."
                      
    redirect_to action: 'show', id: params["from_ngo"]

    
  end

  private

    def ngo_params
      params.require(:ngo).permit(:name, :bir_cert_no, :description)
    end
    
    
    
end
