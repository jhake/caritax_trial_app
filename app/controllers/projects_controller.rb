class ProjectsController < ApplicationController
  def new
    @project = Project.new
  end
  
  def show
    @project = Project.find(params[:id])
  end
  
  def create
    @project = Project.new(project_params)
    if @project.save
      flash[:success] = "New Project Added!"
      redirect_to @project
    else
      render 'new'
    end
  end

  private

    def project_params
      params.require(:project).permit(:name, :short_desc, :description)
    end
    
end
