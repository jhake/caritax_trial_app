module ApplicationHelper

  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = "Caritax"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end
end

class TokenCreateInput
  attr_accessor :number, :exp_month, :exp_year, :cvc, :billing
  
  def initialize
    @number = nil
    @exp_month = nil
    @exp_year = nil
    @cvc = nil
    @billing = {
      :line1 => nil,
      :line2 => nil,
      :city => nil,
      :state => nil,
      :postal_code => nil,
      :country => nil,
      :name => nil,
      :email => nil,
      :phone => nil
    }
  end
  
end

class PaymentCreateInput
  attr_accessor :amount, :currency, :description, :source, :statement_descriptor
  
  def initialize
    @amount = nil
    @currency = nil
    @description = nil
    @source = {
      :id => nil,
      :type => nil
    }
    @statement_descriptor = nil
  end
  
end

class TokenObject
  attr_accessor :valid, :id, :errors
  
  def initialize
    @valid = false
    @id = nil
    @errors = nil
  end
  
  def create(input)
    
    if input.class != TokenCreateInput
      return false
    end
    
    require 'net/http'
    
    uri = URI("https://api.paymongo.com/v1/tokens/") 
    http = Net::HTTP.new(uri.host, uri.port)

    http.use_ssl = true
    req = Net::HTTP::Post.new(uri.path, {'Content-Type' =>'application/json'})
    req.basic_auth 'sk_live_QpCoow6boLqrY6e8jWJori4U', 'pass'
    req.body = {
    	"data" => {
    		"attributes" => {
    			"number" => input.number,
    			"exp_month" => input.exp_month.to_i,
    			"exp_year" => input.exp_year.to_i,
    			"cvc" => input.cvc.to_str		
    		}
    	}
    }.to_json
    
    resp = http.request(req)
    
    if resp.kind_of? Net::HTTPSuccess
      #update object
      @id = (JSON.parse resp.body)["data"]["id"]
      @valid = true
      @errors = nil
      return true
    else
      @errors = (JSON.parse resp.body)["errors"]
      return false
    end

  end
  
  def valid?
    return @valid
  end
end

class PaymentObject
  attr_accessor :amount, :billing, :created, :currency, :description,
    :external_reference_number, :fee, :livemode, :net_amount,
    :statement_descriptor, :status, :errors
    
  def initialize
    @amount = nil
    @billing = nil
    @created = nil
    @currency = nil
    @description = nil
    @external_reference_number = nil
    @fee = nil
    @livemode = nil
    @net_amount = nil
    @statement_descriptor = nil
    @status = nil
    @errors = nil
  end
  
  def create(input)
    
    if input.class != PaymentCreateInput
      return false
    end
    
    require 'net/http'
    
    uri = URI("https://api.paymongo.com/v1/payments") 
    http = Net::HTTP.new(uri.host, uri.port)
    
    http.use_ssl = true
    req = Net::HTTP::Post.new(uri.path, {'Content-Type' =>'application/json'})
    req.basic_auth 'sk_live_QpCoow6boLqrY6e8jWJori4U', 'pass'
    req.body = {
    	"data" => {
    		"attributes" => {
    			"amount" => input.amount.to_i,
    			"currency" => input.currency,
    			"description" => input.description,
    			"source" => input.source,
    			"statement_descriptor" => input.statement_descriptor
    		}
    	}
    }.to_json
    
    resp = http.request(req)
    data = JSON.parse resp.body
    
    if resp.kind_of? Net::HTTPSuccess
      #update object
      @amount = data["amount"]
      @billing = data["billing"]
      @created = data["created"]
      @currency = data["currency"]
      @description = data["description"]
      @external_reference_number = data["external_reference_number"]
      @fee = data["fee"]
      @livemode = data["livemode"]
      @net_amount = data["net_amount"]
      @status = data["status"]
      @errors = nil
      return true
    else
      @errors = (JSON.parse resp.body)["errors"]
      return false
    end
    
  end
  
end