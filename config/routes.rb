Rails.application.routes.draw do

  root 'test_pages#home'
  get '/qwertyuiop', to: 'ngos#new'

  get '/home',      to: 'test_pages#help'
  get '/about',     to: 'test_pages#about'
  get '/contact',     to: 'test_pages#contact'
  
  get '/partners',   to: 'ngos#index'
  get '/addproject', to: 'projects#new'
  
  get '/ngos/*id/donate', to: 'ngos#donate', as: 'ngos_donate'
  post '/donate/makepayment',     to: 'ngos#make_donation'
  
  resources :projects, :except => [:new, :index]
  resources :ngos, :except => [:new]
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
