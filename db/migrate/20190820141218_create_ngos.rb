class CreateNgos < ActiveRecord::Migration[5.1]
  def change
    create_table :ngos do |t|
      t.string :name
      t.text :description
      t.string :bir_cert_no

      t.timestamps
    end
  end
end
