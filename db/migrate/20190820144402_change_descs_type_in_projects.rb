class ChangeDescsTypeInProjects < ActiveRecord::Migration[5.1]
  def change
    change_column :projects, :description, :text
    change_column :projects, :short_desc, :text
  end
end
