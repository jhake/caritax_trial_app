class MakeBirCertNoToIndexInNgos < ActiveRecord::Migration[5.1]
  def change
    add_index :ngos, :bir_cert_no
  end
end
